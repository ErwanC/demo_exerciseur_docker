Produit de deux entiers
------------------------

Écrivez une fonction ``produit`` prenant en entrée deux entiers et renvoyant le produit des deux entiers.

.. easypython:: /exercices/produit/
   :language: TestsPython
   :tags: 
     - test
     - erwan
   :extra_yaml:
     nom_module: produit
